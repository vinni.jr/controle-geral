const routes = [
  {
    path: "/",
    component: () => import("layouts/Default.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      {
        name: "lista",
        path: "lista",
        component: () => import("pages/Lista.vue")
      },
      {
        name: "cadastro",
        path: "cadastro",
        component: () => import("pages/Cadastro.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
